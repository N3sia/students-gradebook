using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Student
{
    class NameYearException : ApplicationException
    {
        public NameYearException(string message) : base("Invalid name or date of birth: " + message) { }
    }
    class GradeException : ApplicationException
    {
        public GradeException(string message) : base("Invalid grade: " + message) { }
    }
    class Student
    {
        #region properties
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private int _dateOfBirth;

        public int DateOfBirth
        {
            get { return _dateOfBirth; }
            set { _dateOfBirth = value; }
        }
        public List<float> grade = new List<float>();

        #endregion
        public Student()
        {
            _name = null;
            _dateOfBirth = 0;
        }
        public Student(string name, int dateOfBirth)
        {
            {
                _name = name;
                _dateOfBirth = dateOfBirth;
            }
        }

        public void Age(Student s)
        {
           int yearnow = DateTime.Now.Year;
            Console.WriteLine(yearnow - s.DateOfBirth);
        }

        public void AddGrade(Student s, float grade)
        {
            s.grade.Add(grade);
        }

        public void PrintGrade(Student s)
        {
            for(int i = 0; i <grade.Count; i++)
            {
                Console.WriteLine(grade[i]);
            }
        }
       
    }
    class Program
    {
        public static List<Student> allstudents = new List<Student>();
        public static void ShowStudents()
        {
            for (int i = 0; i < allstudents.Count; i++)
            {
                Console.WriteLine(i + " - " + allstudents[i].Name);
            }
            Console.WriteLine("**************");
        }
        public static Student ChooseStudent(int c)
        {
            return allstudents[c];
        }

        static void ShowMenu()
        {
            Console.WriteLine("\n");

            Console.WriteLine("1 - Add Student");
            Console.WriteLine("2 - Add Grade");
            Console.WriteLine("3 - Show Grades");
            Console.WriteLine("4 - Show Best Grade");
            Console.WriteLine("5 - Delete Grade");
            Console.WriteLine("6 - Average Grade / Student");
            Console.WriteLine("7 - Summary ");
        }
        static void ValidateName(string name, int dateOfBirth)
        {
            if (name.Length < 1) 
            {
                throw new NameYearException("The name should be at least 2 chars long, and date of birth at least year 1951");
            }
            if (dateOfBirth < 1950)
            {
                throw new NameYearException("The name should be at least 2 chars long, and date of birth at least year 1951");
            }
        }
        static void ValidateGrade(float grade)
        {
            if(grade<1)
            {
                throw new GradeException("Grade should be numbers from 1 to 6");
            }
            if (grade > 6)
            {
                throw new GradeException("Grade should be numbers from 1 to 6");
            }
        }

        static void Main(string[] args)
        {
            ShowMenu();
            int menu = Convert.ToInt32(Console.ReadLine());
            do
            {
                switch (menu)
                {
                    case 1:
                        Console.WriteLine("Enter name of the student: ");
                        string name = Console.ReadLine();
                        Console.WriteLine("Enter date of birth: ");
                        int date = Convert.ToInt32(Console.ReadLine());
                        try
                        {
                            ValidateName(name, date);
                            allstudents.Add(new Student(name, date));
                        }
                        catch (NameYearException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 2:
                        if (allstudents.Count != 0)
                        {
                            Console.WriteLine("Choose student from the list: \n");
                            ShowStudents();
                            int choose = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Choose a grade for student " + ChooseStudent(choose).Name);
                            float grade = Convert.ToSingle(Console.ReadLine());
                            try
                            {
                                ValidateGrade(grade);
                                allstudents[choose].AddGrade(allstudents[choose], grade);
                            }
                            catch (GradeException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            
                        }
                        else Console.WriteLine("Students list is empty. Add students first!");
                       break;
                    case 3:
                        Console.WriteLine("Choose students in case of printing grades \n");
                        ShowStudents();
                        int choose1 = Convert.ToInt32(Console.ReadLine());
                        for (int i = 0; i < ChooseStudent(choose1).grade.Count; i++)
                        {
                            Console.Write(ChooseStudent(choose1).grade[i] + "; ");
                        }
                        Console.WriteLine("\n");
                        break;
                    case 4:
                        Console.WriteLine("Choose students in case of printing the best grade \n");
                        ShowStudents();
                        int choose2 = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine(ChooseStudent(choose2).grade.Max());
                        break;
                    case 5:
                        Console.WriteLine("Choose student to display grades ");
                        ShowStudents();
                        int choose3 = Convert.ToInt32(Console.ReadLine());
                            for (int i = 0; i < ChooseStudent(choose3).grade.Count; i++)
                        {
                            Console.Write("grade number "+ i + " - "+ChooseStudent(choose3).grade[i]);
                        }
                        Console.WriteLine("Which grade should be deleted? :");
                        int chooseGrade = Convert.ToInt32(Console.ReadLine());
                        ChooseStudent(choose3).grade.RemoveAt(chooseGrade);
                        for (int i = 0; i < ChooseStudent(choose3).grade.Count; i++)
                        {
                            Console.Write(ChooseStudent(choose3).grade[i] + ", ");
                        }
                        Console.Write("\n");
                        break;
                    case 6:
                        Console.WriteLine("Choose student to display average grade ");
                        ShowStudents();
                        int choose4 = Convert.ToInt32(Console.ReadLine());
                        float total = ChooseStudent(choose4).grade.Sum();
                        float amountOfPieces = ChooseStudent(choose4).grade.Count();
                        float result = total / amountOfPieces;
                        Console.WriteLine(ChooseStudent(choose4).Name + " average grade is " + result);
                        break;
                    case 7:
                        Console.WriteLine("Summary: ");
                        for (int i = 0; i < allstudents.Count; i++)
                        {
                            Console.WriteLine("No. " + i + " Name: " + allstudents[i].Name + " Date of birth " + allstudents[i].DateOfBirth);
                        }
                        break;
                }
                ShowMenu();
                menu = Convert.ToInt32(Console.ReadLine());
            } while (menu!=0);
            
        }
    }
}
